FROM node:carbon
VOLUME /bot
WORKDIR /bot
RUN npm i -g nodemon
CMD npm i && nodemon main.js