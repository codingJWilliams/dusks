const { AkairoClient } = require('discord-akairo');
const config = require(`./config.json`)

const client = new AkairoClient({
    ownerID: '198106567436926976',
    prefix: config.prefix,
    commandDirectory: './src/commands/'
}, {
    disableEveryone: true,
    allowMention: false,
    blockBots: false
});

client.on("ready", async () => {
    console.log(`${client.user.username} now ready.`);
});

client.build() // Have to build before adding something

/**
 * Custom command resolver to alleviate weird issues where if a VC shared
 * *** it's name with a text channel or role, weird bugs would happen ***
 */
client.commandHandler.resolver.addType('customVoiceChannel', async (word, message) => {
    if (!word) return null;
    var veecees = message.guild.channels.filter( c => c.type == "voice") // Only look at voice channels, nothing else
    if (veecees.get(word)) return message.guild.channels.get(word) // If it's an ID, prioritize that
    if (veecees.find("name", word)) return message.guild.channels.find("name", word) // If it's an exact name, return that

    // Go through each vc, looping through the chars until we find one that doesn't match
    // Track which vc out of all has the most chars at the start shared with the input
    // return that
    var mostMatching = [0, null];
    veecees.forEach(vc => {
        var ind = 0;

        // Case insensitive
        while (vc.name.toLowerCase().startsWith(word.substring(0, ind).toLowerCase()) {
            ind++;
        }
        if (ind > mostMatching[0]) mostMatching = [ind, vc]
    })
    if (mostMatching[1]) return mostMatching[1]
    return null
})

// Login as normal
client.login(config.token);