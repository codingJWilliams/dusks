const { Command } = require('discord-akairo');

class DumpCommand extends Command {
    constructor() {
        super('dump', {
           aliases: ['dump'],
           channelRestriction: 'guild',
           args: [{ id: "target", type: "customVoiceChannel", match: "rest"}]
        });
    }
    async exec(message, { target }) {
    console.log(target.name);
    let userChannel = target
    if(!target) return message.channel.send("Invalid Channel Name/ID, " + userChannel + " is not a valid Voice Channel");
    let i = 0;
    let listUsers = "```";
    let users = userChannel.members.map(m => m.user.tag)
    let userId = userChannel.members.map(m => m.user.id)
    if(!users) return message.channel.send("No members in channel " + userChannel + ".");
    for(i = 0; i <users.length; i++)
    {
        listUsers = listUsers.concat("\nUser: " + users[i] + " ID: " + userId[i]);
    }
    listUsers = listUsers.concat("```")
    if(listUsers === "``````") return message.channel.send("No members in Channel " + userChannel.name + ".");
    message.channel.send(listUsers);
    }
}
    module.exports = DumpCommand;