const { Command } = require('discord-akairo');

class AvatarCommand extends Command {
    constructor() {
        super('avatar', {
           aliases: ['avatar'],
           channelRestriction: 'guild',
           args: [{ id: "target", type: "member" }]
        });
    }

    async exec(message, { target }) {
        let generatingMsg = await message.channel.send("Generating avatar...");
        let userAvatar = target.user || message.author; 
        await message.channel.send("This is " + userAvatar.username + "'s avatar", {
                files: [
                    {
                        attachment: userAvatar.displayAvatarURL, 
                        name: "avatar.png"
                    }
                ]});
        await generatingMsg.delete()
    }
}

module.exports = AvatarCommand;