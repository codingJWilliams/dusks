const { Command } = require('discord-akairo');

class PingCommand extends Command {
    constructor() {
        super('ping', {
           aliases: ['ping'],
           ownerOnly: true,
           channelRestriction: 'guild'
        });
    }

    exec(message) {
        return message.reply('Pong!');
    }
}

module.exports = PingCommand;